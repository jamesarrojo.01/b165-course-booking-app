// [SECTION] Dependencies and Modules
	const auth = require("../auth")
	const User = require('../models/User')
	const Course = require('../models/Course')
	const bcrypt = require("bcrypt");
	const dotenv = require("dotenv")

// [SECTION] Environment Setup
	dotenv.config();
  	const salt = Number(process.env.SALT);


// [SECTION] Functionalities [CREATE]
	module.exports.registerUser = (data) => {

		let fName = data.firstName;
	  	let lName = data.lastName;
	  	let email = data.email;
	  	let passW= data.password;
	  	let gendr = data.gender;
	  	let mobil = data.mobileNo;
	  	
	  	let newUser = new User({
	  		firstName: fName,
	  		lastName: lName,
	  		email: email,
	  		password: bcrypt.hashSync(passW, salt),
	  		gender: gendr,
	  		mobileNo: mobil 
	  	}); 

	  	return newUser.save().then((user, rejected) => {
	  		
	  		if (user) {
	  			return user;
	  		} else {
	  			return 'Failed to Register a new account'; 
	  		}; 
	  	});

	};
	// redundant???
	// module.exports.loginUser = (userData) => {

	// 	let email = userData.email;
	// 	let passW = userData.password;

	// 	return User.findOne({email: email}).then(result => {
	// 		if (result === null) {
	// 			return 'Email Not Found';
	// 		} else {
	// 			return 'Email Found';
	// 		}
	// 	})
	// }

// LOGIN
	module.exports.loginUser = (req, res) => {

		User.findOne({email: req.body.email})
		.then(foundUser => {

			if (foundUser === null) {
				return res.send({ message: "User Not Found" })
			} else {
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

				if(isPasswordCorrect) {
					return res.send({accessToken: auth.createAccessToken(foundUser)})
				} else {
					return res.send(false)
				}
			}

		})
		.catch(err => res.send(err))
	}

// GET USER DETAILS
	module.exports.getUserDetails = (req, res) => {
		console.log(req.user)

		User.findById(req.user.id)
		.then(result => res.send(result))
		.catch(err => res.send(err))
	};




// [SECTION] Functionalities [RETRIEVE]
// [SECTION] Functionalities [UPDATE]
// [SECTION] Functionalities [DELETE]

// ENROLL A REGISTERED USER

	module.exports.enroll = async (req, res) => {
		console.log(req.user.id)
		console.log(req.body.courseId)

		// process stops here and sends response IF user is an admin
		if(req.user.isAdmin) {
			return res.send("Action Forbidden")
		}

		let isUserUpdated = await User.findById(req.user.id).then(user => {
			// Add the courseId in an object and push that object into the user's enrollment array:
			let newEnrollment = {
				courseId: req.body.courseId
			}
			// access the enrollments array from our user and push the new enrollment object into the array
			user.enrollments.push(newEnrollment)
			return user.save().then(user => true).catch(err => err.message)
		})
		if(isUserUpdated !== true) {
			return res.send({message: isUserUpdated})
		}

		// Find the course we will push for our enrollee array
		let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {

			let enrollee = {
				userId: req.user.id
			}

			course.enrollees.push(enrollee)

			return course.save().then(course => true).catch(err => err.message)
		})

		// Stops the process if there is an error saving our course document
		if(isCourseUpdated !== true) {
			return res.send({message: isCourseUpdated})
		}

		if(isUserUpdated && isCourseUpdated) {
			return res.send({message: "Enrolled Successfully."})
		}
	}

	module.exports.getEnrollments = (req, res) => {
		User.findById(req.user.id).then(result => res.send(result.enrollments)).catch(err => res.send(err))
	}