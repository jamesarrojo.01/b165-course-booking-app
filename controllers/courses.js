// [SECTION] Dependencies and Modules
	const Course = require('../models/Course')

// [SECTION] Functionality [CREATE]
	module.exports.createCourse = (info) => {
		let cName = info.name;
		let cDesc = info.description;
		let cCost = info.price;
		let newCourse = new Course({
			name: cName,
			description: cDesc,
			price: cCost
		}) 
		return newCourse.save().then((savedCourse, error) => {
			if (error) {
				return 'Failed to Save New Document'
			} else {
				return savedCourse;
			}

		});
	};

// [SECTION] Functionality [RETRIEVE]
	module.exports.getAllCourse = () => {
		return Course.find({}).then(outComeNiFind => {
			return outComeNiFind;
		})
	}

	module.exports.getCourse = (id) => {
		return Course.findById(id).then(resultOfQuery => {
			return resultOfQuery;
		})
	}

	module.exports.getAllActiveCourse = () => {
		return Course.find({isActive: true}).then(resultOfQuery => {
			return resultOfQuery;
		})
	}

// [SECTION] Functionality [UPDATE]
	module.exports.updateCourse = (id, details) => {
		let cName = details.name;
		let cDesc = details.description;
		let cCost = details.price;
		
		let updatedCourse = {
			name: cName,
			description: cDesc,
			price: cCost
		}
		return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
			if (err) {
				return 'Failed to update Course';
			} else {
				return 'Successfully Updated Course';
			}
		})
	}

	module.exports.deactivateCourse = (id) => {
		let updates = {
			isActive: false
		}
		return Course.findByIdAndUpdate(id, updates).then((archived, error) => {
			if (archived) {
				return `The Course ${id} has been deactivated`;
			} else {
				return `Failed to archive course`;
			}
		})
	}

	module.exports.reactivateCourse = (id) => {
		let updates = {
			isActive: true
		}
		return Course.findByIdAndUpdate(id, updates).then((restored, err) => {
			if (err) {
				return `Failed to restore course`
			} else {
				return `The Course ${id} has been restored`;
			}
		})
	}

// [SECTION] Functionality [DELETE]
	module.exports.deleteCourse = (id) => {
		return Course.findByIdAndRemove(id).then((removedCourse, err) => {
			if (err) {
				return 'No Course Was Removed';
			} else {
				return 'Course Successfully Deleted';
			}
		})
	}